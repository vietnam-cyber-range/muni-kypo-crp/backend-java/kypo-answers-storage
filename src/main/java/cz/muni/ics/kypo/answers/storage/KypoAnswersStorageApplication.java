package cz.muni.ics.kypo.answers.storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KypoAnswersStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(KypoAnswersStorageApplication.class, args);
    }

}
